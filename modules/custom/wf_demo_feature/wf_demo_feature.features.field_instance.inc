<?php
/**
 * @file
 * wf_demo_feature.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function wf_demo_feature_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-advanced_demo_1-body'
  $field_instances['node-advanced_demo_1-body'] = array(
    'bundle' => 'advanced_demo_1',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'node-advanced_demo_1-field_image'
  $field_instances['node-advanced_demo_1-field_image'] = array(
    'bundle' => 'advanced_demo_1',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image',
    'label' => 'Image',
    'required' => FALSE,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'wysiwyg_fields',
      'settings' => array(
        'allowed_schemes' => array(
          'private' => 0,
          'public' => 'public',
        ),
        'allowed_types' => array(
          'audio' => 0,
          'document' => 0,
          'image' => 'image',
          'video' => 0,
        ),
        'browser_plugins' => array(
          'media_default--media_browser_1' => 0,
          'media_default--media_browser_my_files' => 0,
          'upload' => 0,
        ),
        'wysiwyg_fields' => array(
          'advanced_tab' => 1,
          'formatters' => array(
            'image' => 'image',
          ),
          'icon' => array(
            'bundle' => 'lullacons_pack1',
            'icon' => 'photo-multiple-option-add',
          ),
          'label' => '',
          'widget_type' => 'media_generic',
        ),
      ),
      'type' => 'wysiwyg_fields',
      'weight' => -2,
    ),
  );

  // Exported field_instance: 'node-advanced_demo_1-field_table'
  $field_instances['node-advanced_demo_1-field_table'] = array(
    'bundle' => 'advanced_demo_1',
    'default_value' => array(
      0 => array(
        'tablefield' => array(
          'cell_0_0' => '',
          'cell_0_1' => '',
          'cell_0_2' => '',
          'cell_0_3' => '',
          'cell_0_4' => '',
          'cell_1_0' => '',
          'cell_1_1' => '',
          'cell_1_2' => '',
          'cell_1_3' => '',
          'cell_1_4' => '',
          'cell_2_0' => '',
          'cell_2_1' => '',
          'cell_2_2' => '',
          'cell_2_3' => '',
          'cell_2_4' => '',
          'cell_3_0' => '',
          'cell_3_1' => '',
          'cell_3_2' => '',
          'cell_3_3' => '',
          'cell_3_4' => '',
          'cell_4_0' => '',
          'cell_4_1' => '',
          'cell_4_2' => '',
          'cell_4_3' => '',
          'cell_4_4' => '',
          'import' => array(
            'file' => '',
            'import' => 'Upload CSV',
          ),
          'rebuild' => array(
            'count_cols' => 5,
            'count_rows' => 5,
            'rebuild' => 'Rebuild Table',
          ),
        ),
        'wysiwyg_fields' => array(
          'delta' => 0,
          'select' => 0,
        ),
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'tablefield',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_table',
    'label' => 'Table',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'wysiwyg_fields',
      'settings' => array(
        'wysiwyg_fields' => array(
          'advanced_tab' => 0,
          'formatters' => array(
            'custom_formatters_tablefield_pie_chart' => 'custom_formatters_tablefield_pie_chart',
            'tablefield_default' => 'tablefield_default',
          ),
          'icon' => array(
            'bundle' => 'lullacons_pack1',
            'icon' => 'database-option-add',
          ),
          'label' => '',
          'widget_type' => 'tablefield',
        ),
      ),
      'type' => 'wysiwyg_fields',
      'weight' => 0,
    ),
  );

  // Exported field_instance: 'node-advanced_demo_1-field_view'
  $field_instances['node-advanced_demo_1-field_view'] = array(
    'bundle' => 'advanced_demo_1',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'viewfield',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_view',
    'label' => 'View',
    'required' => FALSE,
    'settings' => array(
      'allowed_views' => array(
        'media_default' => 0,
      ),
      'force_default' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'wysiwyg_fields',
      'settings' => array(
        'wysiwyg_fields' => array(
          'advanced_tab' => 1,
          'formatters' => array(
            'viewfield_default' => 'viewfield_default',
          ),
          'icon' => array(
            'bundle' => 'lullacons_pack1',
            'icon' => 'doc-option-add',
          ),
          'label' => '',
          'widget_type' => 'viewfield_select',
        ),
      ),
      'type' => 'wysiwyg_fields',
      'weight' => -1,
    ),
  );

  // Exported field_instance: 'node-basic_demo_1-body'
  $field_instances['node-basic_demo_1-body'] = array(
    'bundle' => 'basic_demo_1',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(
          'trim_length' => 600,
        ),
        'type' => 'text_summary_or_trimmed',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'body',
    'label' => 'Body',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => TRUE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => -4,
    ),
  );

  // Exported field_instance: 'node-basic_demo_1-field_image'
  $field_instances['node-basic_demo_1-field_image'] = array(
    'bundle' => 'basic_demo_1',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image',
    'label' => 'Image',
    'required' => FALSE,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'wysiwyg_fields',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
        'wysiwyg_fields' => array(
          'advanced_tab' => 0,
          'formatters' => array(
            'image' => 'image',
            'lightbox2__lightbox__thumbnail__original' => 'lightbox2__lightbox__thumbnail__original',
            'picture' => 'picture',
          ),
          'icon' => array(
            'bundle' => 'lullacons_pack1',
            'icon' => 'photo-option-add',
          ),
          'label' => '',
          'widget_type' => 'image_image',
        ),
      ),
      'type' => 'wysiwyg_fields',
      'weight' => -3,
    ),
  );

  // Exported field_instance: 'node-basic_demo_1-field_text'
  $field_instances['node-basic_demo_1-field_text'] = array(
    'bundle' => 'basic_demo_1',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_text',
    'label' => 'Text',
    'required' => FALSE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'wysiwyg_fields',
      'settings' => array(
        'size' => 60,
        'wysiwyg_fields' => array(
          'advanced_tab' => 0,
          'formatters' => array(
            'custom_formatters_text_blockquote' => 'custom_formatters_text_blockquote',
            'text_default' => 'text_default',
          ),
          'icon' => array(
            'bundle' => 'lullacons_pack1',
            'icon' => 'doc-option-add',
          ),
          'label' => '',
          'widget_type' => 'text_textfield',
        ),
      ),
      'type' => 'wysiwyg_fields',
      'weight' => -2,
    ),
  );

  // Exported field_instance: 'node-basic_demo_1-field_youtube'
  $field_instances['node-basic_demo_1-field_youtube'] = array(
    'bundle' => 'basic_demo_1',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'youtube',
        'settings' => array(
          'youtube_autohide' => FALSE,
          'youtube_autoplay' => FALSE,
          'youtube_controls' => FALSE,
          'youtube_height' => NULL,
          'youtube_iv_load_policy' => FALSE,
          'youtube_loop' => FALSE,
          'youtube_showinfo' => FALSE,
          'youtube_size' => '420x315',
          'youtube_width' => NULL,
        ),
        'type' => 'hidden',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_youtube',
    'label' => 'Youtube',
    'required' => FALSE,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'wysiwyg_fields',
      'settings' => array(
        'wysiwyg_fields' => array(
          'advanced_tab' => 0,
          'formatters' => array(
            'custom_formatters_youtube_responsive' => 'custom_formatters_youtube_responsive',
            'youtube_video' => 'youtube_video',
          ),
          'icon' => array(
            'bundle' => 'lullacons_pack1',
            'icon' => 'movie-option-add',
          ),
          'label' => '',
          'widget_type' => 'youtube',
        ),
      ),
      'type' => 'wysiwyg_fields',
      'weight' => -1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Body');
  t('Image');
  t('Table');
  t('Text');
  t('View');
  t('Youtube');

  return $field_instances;
}
