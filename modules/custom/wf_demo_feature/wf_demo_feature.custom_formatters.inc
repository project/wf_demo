<?php
/**
 * @file
 * wf_demo_feature.custom_formatters.inc
 */

/**
 * Implements hook_custom_formatters_defaults().
 */
function wf_demo_feature_custom_formatters_defaults() {
  $export = array();

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'tablefield_pie_chart';
  $formatter->label = 'Pie chart';
  $formatter->description = '';
  $formatter->mode = 'php';
  $formatter->field_types = 'tablefield';
  $formatter->code = '$data = array();
foreach ($items[0][\'tabledata\'] as $row_count => $columns) {
  if ($row_count == 0) {
    $headers = $columns;
    continue;
  }
  foreach ($columns as $col => $value) {
    if ($col == 0) {
      continue;
    }
    if (!isset($data[$headers[$col]])) {
      $data[$headers[$col]] = 0;
    }
    $data[$headers[$col]] += $value;
  }
}

$js_data = array("[\'\', \'\']");
foreach ($data as $name => $value) {
  $js_data[] = "[\'$name\', $value]";
}
$js_data = implode(",\\n", $js_data);

$script = "
google.load(\'visualization\', \'1\', {packages:[\'corechart\']});
google.setOnLoadCallback(drawChart);
function drawChart() {
  var data = google.visualization.arrayToDataTable([
    {$js_data}
  ]);

  var chart = new google.visualization.PieChart(document.getElementById(\'piechart\'));
  chart.draw(data);
}";

drupal_add_js(\'//www.google.com/jsapi\', \'external\');
drupal_add_js($script, \'inline\');
return "<div id=\'piechart\'></div>";';
  $formatter->fapi = '';
  $export['tablefield_pie_chart'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'text_blockquote';
  $formatter->label = 'Blockquote';
  $formatter->description = '';
  $formatter->mode = 'token';
  $formatter->field_types = 'text';
  $formatter->code = '<blockquote>[field_property:value]</blockquote>';
  $formatter->fapi = '';
  $export['text_blockquote'] = $formatter;

  $formatter = new stdClass();
  $formatter->disabled = FALSE; /* Edit this to true to make a default formatter disabled initially */
  $formatter->api_version = 2;
  $formatter->name = 'youtube_responsive';
  $formatter->label = 'YouTube responsive';
  $formatter->description = '';
  $formatter->mode = 'formatter_preset';
  $formatter->field_types = 'youtube';
  $formatter->code = 'a:2:{s:9:"formatter";s:13:"youtube_video";s:8:"settings";a:9:{s:12:"youtube_size";s:10:"responsive";s:13:"youtube_width";s:0:"";s:14:"youtube_height";s:0:"";s:16:"youtube_autoplay";i:0;s:12:"youtube_loop";i:0;s:16:"youtube_showinfo";i:0;s:16:"youtube_controls";i:0;s:16:"youtube_autohide";i:0;s:22:"youtube_iv_load_policy";i:0;}}';
  $formatter->fapi = '';
  $export['youtube_responsive'] = $formatter;

  return $export;
}
