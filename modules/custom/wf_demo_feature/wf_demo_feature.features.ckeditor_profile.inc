<?php
/**
 * @file
 * wf_demo_feature.features.ckeditor_profile.inc
 */

/**
 * Implements hook_ckeditor_profile_defaults().
 */
function wf_demo_feature_ckeditor_profile_defaults() {
  $data = array(
    'CKEditor Global Profile' => array(
      'name' => 'CKEditor Global Profile',
      'settings' => array(
        'skin' => 'moono',
        'ckeditor_path' => '%l/ckeditor',
        'ckeditor_local_path' => '',
        'ckeditor_plugins_path' => '%l/plugins',
        'ckeditor_plugins_local_path' => '',
        'ckfinder_path' => '%m/ckfinder',
        'ckfinder_local_path' => '',
        'ckeditor_aggregate' => 'f',
        'toolbar_wizard' => 't',
        'loadPlugins' => array(),
      ),
      'input_formats' => array(),
    ),
    'Wysiwyg' => array(
      'name' => 'Wysiwyg',
      'settings' => array(
        'ss' => 2,
        'toolbar' => '[
    [\'Format\',\'Bold\',\'Italic\',\'-\',\'NumberedList\',\'BulletedList\',\'-\',\'Link\',\'Unlink\'],
    [\'wysiwyg_fields-node-basic_demo_1-field_image\',\'wysiwyg_fields-node-advanced_demo_1-field_image\',\'wysiwyg_fields-node-basic_demo_1-field_text\',\'wysiwyg_fields-node-advanced_demo_1-field_view\',\'wysiwyg_fields-node-basic_demo_1-field_youtube\',\'wysiwyg_fields-node-advanced_demo_1-field_table\'],
    [\'Source\',\'Maximize\']
]',
        'expand' => 't',
        'default' => 't',
        'show_toggle' => 't',
        'uicolor' => 'default',
        'uicolor_user' => 'default',
        'width' => '100%',
        'lang' => 'en',
        'auto_lang' => 't',
        'language_direction' => 'default',
        'allowed_content' => 't',
        'extraAllowedContent' => '',
        'enter_mode' => 'p',
        'shift_enter_mode' => 'br',
        'font_format' => 'p;div;pre;address;h1;h2;h3;h4;h5;h6',
        'custom_formatting' => 'f',
        'formatting' => array(
          'custom_formatting_options' => array(
            'indent' => 'indent',
            'breakBeforeOpen' => 'breakBeforeOpen',
            'breakAfterOpen' => 'breakAfterOpen',
            'breakAfterClose' => 'breakAfterClose',
            'breakBeforeClose' => 0,
            'pre_indent' => 0,
          ),
        ),
        'css_mode' => 'theme',
        'css_path' => '',
        'css_style' => 'theme',
        'styles_path' => '',
        'filebrowser' => 'none',
        'filebrowser_image' => '',
        'filebrowser_flash' => '',
        'UserFilesPath' => '%b%f/',
        'UserFilesAbsolutePath' => '%d%b%f/',
        'forcePasteAsPlainText' => 'f',
        'html_entities' => 'f',
        'scayt_autoStartup' => 'f',
        'theme_config_js' => 'f',
        'js_conf' => '',
        'loadPlugins' => array(
          'drupalbreaks' => array(
            'name' => 'drupalbreaks',
            'desc' => 'Plugin for inserting Drupal teaser and page breaks.',
            'path' => '%plugin_dir%drupalbreaks/',
            'buttons' => array(
              'DrupalBreak' => array(
                'label' => 'DrupalBreak',
                'icon' => 'images/drupalbreak.png',
              ),
            ),
            'default' => 't',
          ),
          'lineutils' => array(
            'name' => 'lineutils',
            'desc' => 'Plugin file: lineutils',
            'path' => '%plugin_dir_extra%lineutils/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'widget' => array(
            'name' => 'widget',
            'desc' => 'Plugin file: widget',
            'path' => '%plugin_dir_extra%widget/',
            'buttons' => FALSE,
            'default' => 'f',
          ),
          'wysiwyg_fields' => array(
            'name' => 'wysiwyg_fields',
            'desc' => 'Wysiwyg Fields plugin',
            'path' => '%base_path%profiles/wf_demo/modules/contrib/wysiwyg_fields/plugins/ckeditor/',
            'default' => 't',
            'buttons' => array(
              'wysiwyg_fields-node-advanced_demo_1-field_image' => array(
                'icon' => '../../../../../../../../profiles/wf_demo/modules/contrib/icon/bundles/lullacons/photo-multiple-option-add.png',
                'label' => 'Image',
                'row' => 'wysiwyg_fields',
              ),
              'wysiwyg_fields-node-advanced_demo_1-field_view' => array(
                'icon' => '../../../../../../../../profiles/wf_demo/modules/contrib/icon/bundles/lullacons/doc-option-add.png',
                'label' => 'View',
                'row' => 'wysiwyg_fields',
              ),
              'wysiwyg_fields-node-advanced_demo_1-field_table' => array(
                'icon' => '../../../../../../../../profiles/wf_demo/modules/contrib/icon/bundles/lullacons/database-option-add.png',
                'label' => 'Table',
                'row' => 'wysiwyg_fields',
              ),
              'wysiwyg_fields-node-basic_demo_1-field_image' => array(
                'icon' => '../../../../../../../../profiles/wf_demo/modules/contrib/icon/bundles/lullacons/photo-option-add.png',
                'label' => 'Image',
                'row' => 'wysiwyg_fields',
              ),
              'wysiwyg_fields-node-basic_demo_1-field_text' => array(
                'icon' => '../../../../../../../../profiles/wf_demo/modules/contrib/icon/bundles/lullacons/doc-option-add.png',
                'label' => 'Text',
                'row' => 'wysiwyg_fields',
              ),
              'wysiwyg_fields-node-basic_demo_1-field_youtube' => array(
                'icon' => '../../../../../../../../profiles/wf_demo/modules/contrib/icon/bundles/lullacons/movie-option-add.png',
                'label' => 'Youtube',
                'row' => 'wysiwyg_fields',
              ),
            ),
          ),
        ),
      ),
      'input_formats' => array(
        'wysiwyg' => 'Wysiwyg',
      ),
    ),
  );
  return $data;
}
