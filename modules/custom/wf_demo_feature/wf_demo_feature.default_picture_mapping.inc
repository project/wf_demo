<?php
/**
 * @file
 * wf_demo_feature.default_picture_mapping.inc
 */

/**
 * Implements hook_default_picture_mapping().
 */
function wf_demo_feature_default_picture_mapping() {
  $export = array();

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Full width responsive';
  $picture_mapping->machine_name = 'full_width_responsive';
  $picture_mapping->breakpoint_group = 'bootstrap';
  $picture_mapping->mapping = array(
    'custom.user.wide' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_resp_custom_user_wide_1x',
      ),
    ),
    'custom.user.normal' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_resp_custom_user_normal_1x',
      ),
    ),
    'custom.user.narrow' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_resp_custom_user_narrow_1x',
      ),
    ),
    'custom.user.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'full_resp',
      ),
      '1.5x' => array(
        'mapping_type' => '_none',
      ),
      '2x' => array(
        'mapping_type' => '_none',
      ),
    ),
  );
  $export['full_width_responsive'] = $picture_mapping;

  return $export;
}
