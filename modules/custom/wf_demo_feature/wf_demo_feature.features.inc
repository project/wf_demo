<?php
/**
 * @file
 * wf_demo_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wf_demo_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "breakpoints" && $api == "default_breakpoint_group") {
    return array("version" => "1");
  }
  if ($module == "breakpoints" && $api == "default_breakpoints") {
    return array("version" => "1");
  }
  if ($module == "custom_formatters" && $api == "custom_formatters") {
    return array("version" => "2");
  }
  if ($module == "picture" && $api == "default_picture_mapping") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wf_demo_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function wf_demo_feature_image_default_styles() {
  $styles = array();

  // Exported image style: full_resp.
  $styles['full_resp'] = array(
    'label' => 'Full width responsive (mobile)',
    'effects' => array(
      4 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 736,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: full_resp_custom_user_narrow_1x.
  $styles['full_resp_custom_user_narrow_1x'] = array(
    'label' => 'Full width responsive (narrow)',
    'effects' => array(
      3 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 720,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: full_resp_custom_user_normal_1x.
  $styles['full_resp_custom_user_normal_1x'] = array(
    'label' => 'Full width responsive (normal)',
    'effects' => array(
      2 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 940,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: full_resp_custom_user_wide_1x.
  $styles['full_resp_custom_user_wide_1x'] = array(
    'label' => 'Full width responsive (wide)',
    'effects' => array(
      1 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1140,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function wf_demo_feature_node_info() {
  $items = array(
    'advanced_demo_1' => array(
      'name' => t('Advanced demo 1'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'basic_demo_1' => array(
      'name' => t('Basic demo 1'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
