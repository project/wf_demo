<?php
/**
 * @file
 * wf_demo_feature.default_breakpoint_group.inc
 */

/**
 * Implements hook_default_breakpoint_group().
 */
function wf_demo_feature_default_breakpoint_group() {
  $export = array();

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'bootstrap';
  $breakpoint_group->name = 'bootstrap';
  $breakpoint_group->breakpoints = array(
    0 => 'custom.user.wide',
    1 => 'custom.user.normal',
    2 => 'custom.user.narrow',
    3 => 'custom.user.mobile',
  );
  $breakpoint_group->type = 'custom';
  $breakpoint_group->overridden = 0;
  $export['bootstrap'] = $breakpoint_group;

  return $export;
}
