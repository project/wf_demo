<?php
/**
 * @file
 * wf_demo_feature.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function wf_demo_feature_filter_default_formats() {
  $formats = array();

  // Exported format: Wysiwyg.
  $formats['wysiwyg'] = array(
    'format' => 'wysiwyg',
    'name' => 'Wysiwyg',
    'cache' => 0,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'filter_html' => array(
        'weight' => -10,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
      'filter_tokens' => array(
        'weight' => 100,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
