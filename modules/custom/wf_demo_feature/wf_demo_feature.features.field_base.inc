<?php
/**
 * @file
 * wf_demo_feature.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function wf_demo_feature_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'body'
  $field_bases['body'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(
      0 => 'node',
    ),
    'field_name' => 'body',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_with_summary',
  );

  // Exported field_base: 'field_image'
  $field_bases['field_image'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_image',
    'indexes' => array(
      'fid' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'image',
    'settings' => array(
      'default_image' => 0,
      'uri_scheme' => 'public',
    ),
    'translatable' => 0,
    'type' => 'image',
  );

  // Exported field_base: 'field_table'
  $field_bases['field_table'] = array(
    'active' => 1,
    'cardinality' => -1,
    'data' => array(
      'entity_types' => array(),
      'foreign keys' => array(),
      'id' => 8,
      'indexes' => array(),
      'settings' => array(
        'cell_processing' => 0,
        'export' => 0,
        'lock_values' => 0,
        'restrict_rebuild' => 0,
      ),
      'storage' => array(
        'active' => 1,
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_table' => array(
                'format' => 'field_table_format',
                'value' => 'field_table_value',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_table' => array(
                'format' => 'field_table_format',
                'value' => 'field_table_value',
              ),
            ),
          ),
        ),
        'module' => 'field_sql_storage',
        'settings' => array(),
        'type' => 'field_sql_storage',
      ),
      'translatable' => 0,
    ),
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_table',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'tablefield',
    'settings' => array(
      'cell_processing' => 0,
      'export' => 0,
      'lock_values' => 0,
      'restrict_rebuild' => 0,
    ),
    'translatable' => 0,
    'type' => 'tablefield',
  );

  // Exported field_base: 'field_text'
  $field_bases['field_text'] = array(
    'active' => 1,
    'cardinality' => -1,
    'data' => array(
      'entity_types' => array(),
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'id' => 3,
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'settings' => array(
        'max_length' => 255,
      ),
      'storage' => array(
        'active' => 1,
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_text' => array(
                'format' => 'field_text_format',
                'value' => 'field_text_value',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_text' => array(
                'format' => 'field_text_format',
                'value' => 'field_text_value',
              ),
            ),
          ),
        ),
        'module' => 'field_sql_storage',
        'settings' => array(),
        'type' => 'field_sql_storage',
      ),
      'translatable' => 0,
    ),
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_text',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_view'
  $field_bases['field_view'] = array(
    'active' => 1,
    'cardinality' => -1,
    'data' => array(
      'entity_types' => array(),
      'foreign keys' => array(),
      'id' => 7,
      'indexes' => array(),
      'settings' => array(),
      'storage' => array(
        'active' => 1,
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_view' => array(
                'vargs' => 'field_view_vargs',
                'vname' => 'field_view_vname',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_view' => array(
                'vargs' => 'field_view_vargs',
                'vname' => 'field_view_vname',
              ),
            ),
          ),
        ),
        'module' => 'field_sql_storage',
        'settings' => array(),
        'type' => 'field_sql_storage',
      ),
      'translatable' => 0,
    ),
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_view',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'viewfield',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'viewfield',
  );

  // Exported field_base: 'field_youtube'
  $field_bases['field_youtube'] = array(
    'active' => 1,
    'cardinality' => -1,
    'data' => array(
      'entity_types' => array(),
      'foreign keys' => array(),
      'id' => 4,
      'indexes' => array(
        'video_id' => array(
          0 => 'video_id',
        ),
      ),
      'settings' => array(),
      'storage' => array(
        'active' => 1,
        'details' => array(
          'sql' => array(
            'FIELD_LOAD_CURRENT' => array(
              'field_data_field_youtube' => array(
                'input' => 'field_youtube_input',
                'video_id' => 'field_youtube_video_id',
              ),
            ),
            'FIELD_LOAD_REVISION' => array(
              'field_revision_field_youtube' => array(
                'input' => 'field_youtube_input',
                'video_id' => 'field_youtube_video_id',
              ),
            ),
          ),
        ),
        'module' => 'field_sql_storage',
        'settings' => array(),
        'type' => 'field_sql_storage',
      ),
      'translatable' => 0,
    ),
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_youtube',
    'indexes' => array(
      'video_id' => array(
        0 => 'video_id',
      ),
    ),
    'locked' => 0,
    'module' => 'youtube',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'youtube',
  );

  return $field_bases;
}
