core = 7.x
api = 2



; Defaults

defaults[projects][subdir] = contrib
defaults[projects][type] = module



; Modules

projects[breakpoints][version] = 1.3

projects[ckeditor][version] = 1.16
; Add grouping of plugin buttons - https://www.drupal.org/node/2511016#comment-10110826
projects[ckeditor][patch][] = https://www.drupal.org/files/issues/grouping-2511016-1.patch

projects[ctools][version] = 1.7

projects[custom_formatters][download][revision] = 036c8c3
projects[custom_formatters][download][branch] = 7.x-2.x

projects[entity][version] = 1.6

projects[famfamfam][version] = 1.0

projects[features][version] = 2.6

projects[field_tokens][download][revision] = d94c9c7
projects[field_tokens][download][branch] = 7.x-2.x

projects[file_entity][version] = 2.0-beta1

; Dev build used as 1.0-beta5 throws errors during installation.
; @TODO: Revisit when new stable is released.
projects[icon][download][revision] = 32dded9
projects[icon][download][branch] = 7.x-1.x

projects[jquery_update][version] = 3.0-alpha2

projects[libraries][version] = 2.2

projects[lightbox2][version] = 1.0-beta1

; Dev build used as 2.0-alpha4 provides a blank 'destination' step.
; @TODO: Revisit when new stable is released.
projects[media][download][revision] = 9f7142b
projects[media][download][branch] = 7.x-2.x

projects[picture][version] = 2.11
; Fix CTools Export UI plugin definition - https://www.drupal.org/node/2513714#comment-10066310
projects[picture][patch][] = https://www.drupal.org/files/issues/ctools_export_ui_plugin_fix-2513714-1.patch

projects[strongarm][version] = 2.0

projects[tablefield][download][revision] = 43950aa
projects[tablefield][download][branch] = 7.x-2.x

projects[token][version] = 1.6

projects[token_filter][version] = 1.1

projects[token_replace_ajax][download][revision] = 9fab2df
projects[token_replace_ajax][download][branch] = 7.x-1.x

projects[viewfield][version] = 2.0

projects[views][version] = 3.11

projects[wysiwyg_fields][download][revision] = ae37c39
projects[wysiwyg_fields][download][branch] = 7.x-2.x

projects[youtube][version] = 1.6



; Themes

projects[bootstrap][type] = theme
projects[bootstrap][version] = 3.0



; Libraries

libraries[ckeditor][download][type] = file
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.7/ckeditor_4.4.7_standard.zip

; famfamfam.com icons can't be added to distro due to license.
; libraries[famfamfam_silk_icons][download][type] = file
; libraries[famfamfam_silk_icons][download][url] = http://www.famfamfam.com/lab/icons/silk/famfamfam_silk_icons_v013.zip

libraries[lineutils][download][type] = file
libraries[lineutils][download][url] = http://download.ckeditor.com/lineutils/releases/lineutils_4.4.7.zip
libraries[lineutils][directory_name] = plugins/lineutils

libraries[widget][download][type] = file
libraries[widget][download][url] = http://download.ckeditor.com/widget/releases/widget_4.4.7.zip
libraries[widget][directory_name] = plugins/widget
